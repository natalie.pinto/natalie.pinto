# Hello! Welcome to my README!
My name is Natalie Pinto and I’m the Senior Manager of Technical Program Management here at GitLab. You can see more about my background on my [linkedin profile](https://www.linkedin.com/in/pintonatalie). 

## About Me
- I’m from the San Francisco Bay Area and have lived here all my life. I didn’t even go very far for college; I attended [UC Santa Cruz](https://www.ucsc.edu/) (which is debatably part of the Monterey Bay and not the San Francisco Bay, but  it’s very close, geographically speaking). 
- While at UC Santa Cruz I studied History and Literature with a goal of becoming a middle school teacher. Life threw me a curveball and I ended up not pursuing that goal after graduation. Instead I found my way into the startup world and the rest is history (*comedic punchline drum sound*).
- When I’m not at work I spend my time reading, playing escape rooms and board games, gardening, and working on various fiber arts (knitting, weaving, mending, etc). I read a ton, mostly sci-fi and fantasy. I love talking about books and I volunteer at my local library because being surrounded by books is my happy place.
- I’m also on the board of a local theater company called [Silicon Valley Shakespeare](https://www.svshakespeare.org/), where I lead the board’s fundraising efforts.

## Why Program Management? 
- Here are a few of my Program Management principles:
  1. Always be a force multiplier. It’s a Program Manager’s primary goal to facilitate, guide, and smooth the path of their projects. Sometimes that means finding new and creative ways to do things and sometimes it means *avoiding* anything new and disruptive. I trust the team to be experts in their field and it’s my job to find the right way to elevate their work.
  1. A good Program Manager is a neutral party. I’m here to facilitate and help teams find more efficiency and happiness with their work. I can’t do that if I have my own motivations and agendas. I’m aligned to the business goals and not to a specific team, methodology, or person. 
  1. Keep an eye on Communication, Planning, and Dependencies. These tend to be the most challenging parts of managing cross-functional efforts and is often why a Program Manager is pulled into a project in the first place. I want to make sure these pieces are going well so that the rest of the team can focus on their primary areas of expertise. 

- Program Management is the perfect combination of everything I love doing. I get to spend time working on a variety of challenges, projects, and problems. I get to interact with a lot of different people and their various working styles. I have to find ways to be equally effective across all those different challenges and people. I love it! 

## Feedback
- Please don’t hesitate to reach out to me with any feedback, big or small. Because I work with so many different teams and people, it’s vital that I understand if my work is effective with one group, but not another. 
- If you aren’t comfortable providing feedback to me live in-person (for example, in a 1:1 over Zoom), please feel free to send it to me over Slack or email or fill out [this anonymous form](https://docs.google.com/forms/d/e/1FAIpQLScZ7PiTquaiiqtE8gfgg35UJpC4-fi1OqZQBoQaMMbmw5xnFg/viewform). 
- If you would like to ask me for feedback, please allow me some time to think through any feedback I may have for you. I generally like to take time to think about these questions rather than replying off the top of my head. I will try to provide feedback promptly and constructively. 

## Communication
- I check the various communication mediums (Slack, email, GitLab) 2-3 times per day and I follow an “inbox zero” mindset.  I make every effort to reply to people promptly, but please allow 24 hours for me to get back to you. 
- My working hours are 7am - 3pm Pacific time, although I can take meetings outside those hours by request.
